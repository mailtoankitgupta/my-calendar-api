from datetime import datetime

import flask
from flask import request

app = flask.Flask(__name__)
app.config['DEBUG'] = True


schedules = {
    'user1': [],
    'user2': [],
    'user3': [],
    'user4': [],
    'user5': []
}


def is_valid(user_id):
    return user_id in schedules.keys()


def errorUser404(user_id):
    return _corsify_actual_response(flask.jsonify({
        'errorMsg': 'user not found with id: ' + user_id
    })), 404


def has_overlap(start_date1, end_date1, start_date2, end_date2):
    def get_date(date):
        return datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f%z")
    return get_date(start_date1) < get_date(end_date2) and get_date(end_date1) > get_date(start_date2)


def is_conflict(u, schedule):
    for user_schedule in schedules[u]:
        if has_overlap(user_schedule.get('start'), user_schedule.get('end'),
                       schedule.get('start'), schedule.get('end')):
            print("has conflict", user_schedule, schedule)
            return True
    return False


def is_meeting_conflict(schedule):
    users = schedule.get('users')
    for u in users:
        if is_conflict(u, schedule):
            return True
    return False


def _build_cors_prelight_response():
    response = flask.make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response


def _corsify_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route('/api/v1/users/<user_id>/schedule', methods=['GET', 'OPTIONS'])
def get_schedule(user_id):
    if request.method == "OPTIONS":  # CORS preflight
        return _build_cors_prelight_response()
    elif request.method == "GET":
        if is_valid(user_id):
            return _corsify_actual_response(flask.jsonify(schedules[user_id]))
        else:
            return errorUser404(user_id)


def create_schedule(schedule, user_id):
    if not is_valid(user_id):
        return
    elif len(schedules[user_id]) == 0:
        schedule['uid'] = 0
    else:
        schedule['uid'] = schedules[user_id][-1]['uid'] + 1
    schedules[user_id].append(schedule)


@app.route('/api/v1/users/<user_id>/schedule', methods=['POST', 'OPTIONS'])
def save_schedule(user_id):
    if request.method == "OPTIONS":  # CORS preflight
        return _build_cors_prelight_response()
    elif request.method == "POST":
        if is_valid(user_id):
            schedule = request.get_json(silent=True)
            meeting_conflict = is_meeting_conflict(schedule)
            for u in schedule.get('users'):
                create_schedule(schedule, u)  # here we can differentiate the organizer, user_id
            return _corsify_actual_response(flask.jsonify({
                'is_meeting_conflict': meeting_conflict,
                'schedule': schedule
            }))
        else:
            return errorUser404(user_id)


@app.route('/api/v1/schedule/check', methods=['POST', 'OPTIONS'])
def check_schedule():
    if request.method == "OPTIONS":  # CORS preflight
        return _build_cors_prelight_response()
    elif request.method == "POST":
        schedule = request.get_json(silent=True)
        meeting_conflict = is_meeting_conflict(schedule)
        return _corsify_actual_response(flask.jsonify({
            'is_meeting_conflict': meeting_conflict
        }))


app.run()
